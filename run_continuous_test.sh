#!/bin/bash

source ~/.bashrc && source /opt/ros/melodic/setup.bash && source /diagnostics_ws/devel/setup.bash && export ROS_MASTER_URI=http://control:11311 && rosrun pal_continuous_test pal_continuous_test.sh pal_computer_monitor_cfg_raspi `rospack find pal_computer_monitor_cfg_raspi`/config/control
