FROM arm32v7/ros:melodic-perception-bionic
LABEL maintainer="Alex Martinez <alex.martinez@pal-robotics.com>"
ARG WS=/diagnostics_ws
RUN mkdir -p $WS/src
WORKDIR $WS
ARG ROS_DISTRO=melodic
RUN apt-get update; exit 0
RUN apt-get install curl -y
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
RUN apt-get update && apt-get install -y \
  ssh-import-id \
  python-catkin-tools \
  apt-utils \
  libboost-all-dev \
  pluginlib-dev \
  ros-$ROS_DISTRO-diagnostic-updater \
  ros-$ROS_DISTRO-diagnostic-msgs \
  tf \
  topic-tools \
  ros-$ROS_DISTRO-ros-type-introspection \
  ros-$ROS_DISTRO-roscpp \
  ros-$ROS_DISTRO-message-generation \
  ros-$ROS_DISTRO-std-msgs \
  ros-$ROS_DISTRO-actionlib-msgs \
  ros-$ROS_DISTRO-rospy \
  python-statsd \
  python-wstool \
  python-rosinstall \
  python3-vcstool \
  wget \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt -yq install smartmontools; exit 0 \
&& rm -rf /var/lib/apt/lists/*
# Copy your gitlab id_rsa to this directory to checkout, DO NOT COMMIT IT!
COPY id_ed25519 /root/.ssh/id_ed25519
RUN chmod 600 /root/.ssh/id_ed25519
RUN echo "StrictHostKeyChecking no" > /root/.ssh/config
RUN wget https://gitlab.com/validation_1/raspi_diagnostics_docker/-/raw/main/run_continuous_test.sh
RUN wget https://gitlab.com/-/snippets/2161496/raw/main/diagnostics_pkgs.rosinstall
RUN vcs import src < diagnostics_pkgs.rosinstall
# pb18 package is not needed to execute pal_continuous_test. Only need pb18_msgs
RUN rm -rf src/pb18/pb18
RUN /bin/bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash \
  && catkin build -j2'
ENTRYPOINT ["/bin/bash", "-c", "sh /diagnostics_ws/run_continuous_test.sh]
